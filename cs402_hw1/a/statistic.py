import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

read = 0
write = 1
tex_url = "http://www.cs.iit.edu/~virgil/cs470/varia/traces/benchmarks/tex.din"
cc1_url = "http://www.cs.iit.edu/~virgil/cs470/varia/traces/benchmarks/cc1.din"
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)


def get_data(url, limit=None):
    data = pd.read_table(url, sep=' ', names=['rw', 'addr'])[:limit]
    data['addr'] = [str(int(i, 16)) for i in data['addr']]
    return data


def histogram_show(url, data_limit, addr_limit):  # HW1 1(a)
    """
    params:
        url: data url
        data_limit: because of too much data to run slow, set the data limit
        addr_limit: set address limit
    """
    plt.rcParams['font.sans-serif'] = ['Arial Unicode MS']

    data = get_data(url, data_limit)
    addr_list = list(set(data['addr']))[:addr_limit]
    # two columns: address, address count
    addr_count = pd.DataFrame(np.zeros([len(addr_list), 1]), index=addr_list, columns=['count'])

    count_list = []  # address count list

    # get the count of every address
    for i in data['addr']:
        for label in addr_list:
            if str(i).__contains__(str(label)):
                addr_count.loc[label, 'count'] += 1
    for c in addr_count['count']:
        count_list.append(c)

    """
    eg.
        fig = plt.figure # new canvas
        ax = fig.add_subplot(1,1,1) # subgraph initialization
    """
    fig, ax = plt.subplots()
    # Plot the distribution characteristics of qualitative data on the y-axis
    ax.barh(addr_list, count_list)

    for a, b, label in zip(count_list, addr_list, count_list):
        plt.text(a, b, label, ha='center', va='bottom')

    ax.set_xlabel('count')
    ax.set_ylabel('address')
    # ax.set_title('frequency of cc1.din')
    ax.set_title('frequency of tex.din')
    ax.legend()
    plt.show()


def rw_statistic(url):  # HW1 1(b)
    """
    params:
        url: data url
    """
    data = get_data(url)
    return data['rw'].value_counts()


if __name__ == "__main__":
    """HW1 1(a)"""
    # histogram_show(cc1_url, 1000000, 50)
    histogram_show(tex_url, 1000000, 50)

    """HW1 1(b)"""
    # print("the rw frequency of tex.din is:")
    # print(rw_statistic(tex_url))
    # print("==================================")
    # print("the rw frequency of cc1.din is:")
    # print(rw_statistic(cc1_url))
