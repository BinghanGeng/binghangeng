import numpy as np
import time
from cs402_hw1.b.Matrix import Matrix

"""
2.(a)(b) Write a program, using your favorite programming language, 
 that multiplies two rectangular matrices -- please no square matrices
 -- whose elements are randomly generated. 
 You will have two versions of the program, 
 one in which matrix elements are integers 
 and another one where they are real numbers (double) (2x15 points).
"""


def int_matrix(m, n, revert):
    start_time = time.perf_counter()

    T = Matrix(np.random.randint(1, 100, (m, n)))
    P = Matrix(np.random.randint(1, 100, (n, m)))
    if revert:
        P.dot(T)  # 2.(b): Change your multiplication algorithm
    else:
        T.dot(P)

    end_time = time.perf_counter()
    run_time = round((end_time - start_time), 3)
    return run_time


def double_matrix(m, n, revert):
    start_time = time.perf_counter()

    T = Matrix(np.random.rand(m, n))
    P = Matrix(np.random.rand(n, m))
    if revert:
        P.dot(T)  # 2.(b): Change your multiplication algorithm
    else:
        T.dot(P)
    end_time = time.perf_counter()
    run_time = round((end_time - start_time), 3)
    return run_time


def runtime_test(m_type, m, n, revert=False):
    s = 0
    t = 10  # run times
    for i️ in range(t):
        if m_type == 'int':
            rt = int_matrix(m, n, revert)
        else:
            rt = double_matrix(m, n, revert)
        print('No.{num} run time is: {t}'.format(num=i️ + 1, t=rt))
        s += rt
    avg = s / t
    avg = round(avg, 3)
    print('average run time is: {avg}'.format(avg=avg))


if __name__ == "__main__":
    runtime_test('int', 300, 200)  # HW1 2(a)
    print("===================")
    runtime_test('int', 300, 200, True)  # HW1 2(b)
