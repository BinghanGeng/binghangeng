
class Vector:
    def __init__(self, lst):
        self._values = list(lst)

    def dot(self, another):
        """Vector dot product, return result scalar"""
        assert len(self) == len(another), \
            "Error in dot product. Length of vectors must be same."

        return sum(a * b for a, b in zip(self, another))

    def __iter__(self):
        """Return a vector iterator"""
        return self._values.__iter__()

    def __getitem__(self, index):
        """Take the index element of the vector"""
        return self._values[index]

    def __len__(self):
        """Return the length of the vector (how many elements are there)"""
        return len(self._values)

    def __repr__(self):
        return "Vector({})".format(self._values)

    def __str__(self):
        return "({})".format(", ".join(str(e) for e in self._values))
