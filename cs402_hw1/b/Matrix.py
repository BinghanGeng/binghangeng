from cs402_hw1.b.Vector import Vector


class Matrix:

    def __init__(self, list2d):
        self._values = [row[:] for row in list2d]

    def row_vector(self, index):
        """Return the index row vector of the matrix"""
        return Vector(self._values[index])

    def col_vector(self, index):
        """Return the index-th column vector of the matrix"""
        return Vector([row[index] for row in self._values])

    def __repr__(self):
        return "Matrix({})".format(self._values)

    def row_num(self):
        """Return the number of rows in the matrix"""
        return self.shape()[0]

    __len__ = row_num

    def col_num(self):
        """Return the number of columns in the matrix"""
        return self.shape()[1]

    def dot(self, another):
        assert self.col_num != another.row_num(), \
            "Error in Matrix-Matrix Multiplication."
        return Matrix([[self.row_vector(i).dot(another.col_vector(j)) for j in range(another.col_num())]
                       for i in range(self.row_num())])

    def shape(self):
        """Returns the shape of the matrix: (number of rows, number of columns)"""
        return len(self._values), len(self._values[0])

    __str__ = __repr__
