if __name__ == "__main__":

    print("system1 information: ")
    print("\r")
    print("manufacturer: Apple")
    print("CPU type: Dual-core Intel Core i5 @ 2.7GHz")
    print("amount of memory: 8 GB")
    print("operating system: macOS 11.2")
    print("compiler used: Python 3.8.5")
    print("====================================")
    print("system2 information:")
    print("\r")
    print("manufacturer: Microsoft Corporation")
    print("CPU type: Intel(R) Core(TM) i7-10870H CPU @ 2.20GHz 2.21GHz")
    print("amount of memory: 32 GB")
    print("operating system: Windows 10.1.19041")
    print("compiler used: Python 3.8.5")

