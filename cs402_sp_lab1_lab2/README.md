# CS402-SP-LAB1&LAB2-Binghan-Geng
A database of employee, finished by C program.

## table of contents
  - [CS402-SP-LAB1&LAB2-Binghan-Geng](#cs402-sp-lab1lab2-binghan-geng)
  - [table of contents](#table-of-contents)
  - [general info](#general-info)
  - [setup](#setup)
  - [features](#features)
  - [contact](#contact)
  
## general info
I use 4 functions to manage the Employee Information. It can read(using readfile.h) the txt file and store into the struct-person. Then, we can get the detail by "circulate" the list. we can get the information of id by binary search(we have ordered the info by qsort_function). Then, we can search the person by last name. Besides, we can add a employee by scanf(using readfile.h), and the new one also be sorted by qsort_function.

## setup
* IDE:VSCode
* Run with command line in terminal
* 1.gcc readfile.c
* 2../a.out small.txt

## features
* 1.Print the Database
* 2.Lookup by ID
* 3.Lookup by Last Name
* 4.Add an Employee
* 5.Quit
* 6.Remove an employee
* 7.Update an employee's information
* 8.Print the M employees with the highest salaries
* 9.Find all employees with matching last name

## contact
Created by BinghanGeng
Wellcome to contact me with email gengbinghan@gmail.com
