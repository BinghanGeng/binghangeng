# CS402-2021-spring sp-lab3

## table of contents
  - [General info](#general-info)
  - [Setup](#setup)
  - [Features](#features)
  - [Status](#status)
  - [Contacts](#contacts)

## General info
For this lab, you will implement a program that reads data from a file and computes some basic statistical analysis measures for the data set.

## Setup
* IDE:vscode
* Run with command line in terminal.
* 1.gcc basicstats.c -o basicstats
* 2.basicstats.exe small.txt
* 3.basicstats.exe large.txt

## Features
* 1.compute mean of the data set
* 2.compute standard deviation of the data set
* 3.compute median of the data set

## Status
This project is: finished.

## Contacts
Created by BinghanGeng
Wellcome to contact me with email gengbinghan@gmail.com
