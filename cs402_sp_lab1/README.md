# CS402-SP-LAB1-Binghan-Geng
A database of employee, finished by C program.
## File Name ##
Employee Database Program
## Designer #####
CS402_SP_LAB1_Bingahn Geng
## Path #####
C,VSCode,mingw64
## Introduction #####
I use 4 functions to manage the Employee Information. It can read(using readfile.h) the txt file and store into the struct-person. Then, we can get the detail by "circulate" the list. we can get the information of id by binary search(we have ordered the info by qsort_function). Then, we can search the person by last name. Besides, we can add a employee by scanf(using readfile.h), and the new one also be sorted by qsort_function.
## Operate #####
# 1.Prepare a file(.txt)on the path of the C program:
Example:
"165417 Cathryn Danner 72000
273225 Matt Meeden 69000
633976 Martine Marshall 99000
471163 Robert Dufour 91000"
# 2.Operate with VScode:
VScode -- F5
# 3.Input the path of File
./employee_system.exe "file name".txt
