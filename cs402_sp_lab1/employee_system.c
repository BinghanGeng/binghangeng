#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "readfile.h"

int main(int argc, char *argv[]) {
	if (argc < 2) {
		printf("Input your file name(.txt) ..\n");
		return 0;
	}
	char *file_name = argv[1], first_name[RANGE_NAME], last_name[RANGE_NAME];//file_name get
	struct person employee[RANGE_EMP];//creat a struct_employee and limit the space_Range_emp
	int option, employ_id, key, salary, confirm;//define the keys
	int n_emp = 0, list[RANGE_EMP];
	if (open_file(file_name) == -1) {//open the file
		printf("error reading file\n");
		return -1;
	}
	FILE *fp = fopen(file_name, "r");
	while (!feof(fp)) {//circulate the content and give to the list
		fscanf(fp, "%d %s %s %d\n", &employee[n_emp].id, &employee[n_emp].first_name, &employee[n_emp].last_name, &employee[n_emp].salary);
		list[n_emp] = employee[n_emp].id;
		n_emp++;
	}
	qsort(employee,n_emp,sizeof(struct person),ID_cpmpare);//set the order:small to big
	fclose(fp);
	while (1) {//print the info of options
		printf("----------------------------------\n");
		printf("Employee DB Menu:\n");
		printf("----------------------------------\n");
		printf("(1) Print the Database\n");
		printf("(2) Lookup by ID\n");
		printf("(3) Lookup by Last Name\n");
		printf("(4) Add an Employee \n");
		printf("(5) Quit\n");
		printf("----------------------------------*\n");
		printf("Enter your choice: \n");
		scanf("%d", &option);
		if (option == 1) {
			printf("------------------------------------------\n");
			printf("NAME                       SALARY    ID \n");
			printf("------------------------------------------\n");
			for (int i = 0; i < n_emp; i++) {//circulate the list for every employee
				printf("%-10s %-10s %10d %10d\n", employee[i].first_name, employee[i].last_name, employee[i].salary, employee[i].id);
			}
			printf("------------------------------------------\n");
			printf("Number of Employees (%d) \n",n_emp);
		}
		else if (option == 2) {
            printf("Enter a 6 digit employee id: \n");
            read_int(&employ_id);
			int list1[RANGE_EMP];
			for(int i = 0; i <n_emp; i++){
				list1[i] = employee[i].id;
			}
            key = binary_search(list1, 0 , n_emp, employ_id);//binary search to the key,o(n/2)
            if(key == -1){
                printf("Employee with id %d not found \n",employ_id);
            }
            else {
                Key_tostring(employee,key);
			}
		}
		else if (option == 3) {
            printf("Enter Employee's last name (no extra spaces): \n");
            read_string(&last_name);
            key = Last_name(employee,n_emp,last_name);//search one by one to the key
            if(key == -1){
                printf("Employee with name %s not found \n",last_name);
            }
            else{
                Key_tostring(employee,key);
			}
		}
		else if (option == 4) {
            printf("Enter the first name of the employee: \n");
            read_string(&first_name);
            printf("Enter the last name of the employee: \n");
            read_string(&last_name);
            printf("Enter employee's salary (30000 to 150000): \n");
            read_int(&salary);
			printf("do you want to add the following employee to the DB?\n");
			printf("            %s %s, salary: %d                \n",first_name,last_name,salary);
            printf("Enter 1 for yes, 0 for no:\n");
            scanf("%d\n",&confirm);
            if(confirm == 1){
                if((salary>=30000)&&(salary<=150000)){//judge the right content and put it into the struct
					strcpy(employee[n_emp].first_name,first_name);
					strcpy(employee[n_emp].last_name,last_name);
					employee[n_emp].id = employ_id;
					employee[n_emp].salary = salary;
                    n_emp++;
					qsort(employee,n_emp,sizeof(struct person),ID_cpmpare);
                }
                else{
                    printf("Error: false salary!");
                }
            }
            else{
                printf("Sorry, you cancel this ADD_action.");
            }
		}
		else if (option == 5) {
			printf("goodbye! \n");
			break;
		}
		
		else {
			printf("Hey, 10 is not between 1 and 5, try again...\n");
			continue;
		}
	}
}
